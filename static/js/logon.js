(function(){
	'use strict';
	
	angular.module('warbookApp')
		.controller('LogonController', LogonController);
		
		LogonController.$inject = ['$http'];
		
		function LogonController($http){
			var vm = this;
			
			vm.ErrorMessage = "Default error message";
			vm.ShowErrorMessageFlag = false;
			
			vm.SuccessMessage = "Default success message";
			vm.ShowSuccessMessageFlag = false;
			
			vm.FirstName = "";
			vm.LastName = "";
			vm.Gender = 1;
			vm.BirthDate = "";
			vm.EmailAddress = "";
			vm.Password = "";
			vm.PasswordConfirm = "";
			
			vm.RegisterNewUser = RegisterNewUser;
					
			function RegisterNewUser(){
				
				var Uri = "/warbook/logon/RegisterNewUser";
				
				var Data = {
					FirstName: vm.FirstName,
					LastName: vm.LastName,
					Gender: vm.Gender,
					BirthDate: vm.BirthDate,
					EmailAddress: vm.EmailAddress,
					Password: vm.Password
				};
				
				if(vm.Password.length >= 6){
					vm.ShowErrorMessageFlag = false;
					if(vm.Password != vm.PasswordConfirm){
						vm.ErrorMessage = "Password mismatch";
						vm.ShowErrorMessageFlag = true;
					}else{
						vm.ShowErrorMessageFlag = false;
						$http.post(Uri, Data).success(function(){
							vm.ShowSuccessMessageFlag = true;
							vm.SuccessMessage = "Registration completed";
							vm.FirstName = "";
							vm.LastName = "";
							vm.Gender = 1;
							vm.BirthDate = "";
							vm.EmailAddress = "";
							vm.Password = "";
							vm.PasswordConfirm = "";		
						});	
					}	
				}else{
					vm.ErrorMessage = "Password must be at least 6 character length";
					vm.ShowErrorMessageFlag = true;
				}
			}
		}
})();