(function(){
	'use strict';
	
	angular.module('warbookApp')
		.controller("HomeController", HomeController);
		
		HomeController.$inject = ['$http'];
		
		function HomeController($http){
			var vm = this;
			
			
			vm.Post = [];
			vm.Request = [];
			vm.NewPost = "";
			vm.ProfilePic=1;
			vm.UserID=0;
			
			vm.LoadPost = LoadPost;
			vm.Share = Share;
			vm.Up = Up;
			vm.Down = Down;
			vm.LoadRequest = LoadRequest();
			
			LoadPost();
			LoadInfo();
			
			$('#autocomplete').autocomplete({
				serviceUrl: '/warbook/Profile/Users',
				onSelect: function (suggestion) {
					location.href = '/warbook/Profile/view?UserID='+suggestion.data;
				}
			});
			
			function LoadInfo(){
				
				var uri  = "/warbook/Profile/LoadInfo";
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];			
						}
					}
				}	
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.UserID = response.UserID;
					vm.ProfilePic = response.ProfilePic;
				});
			}
			
			function Up(PostID){
				var uri  = "/warbook/Profile/Up?PostID="+PostID;
				var data = { };
					
				$http.post(uri, data).success(function(response){
					LoadPost();
				});	
			}
			
			function Down(PostID){
				var uri  = "/warbook/Profile/Down?PostID="+PostID;
				var data = { };
					
				$http.post(uri, data).success(function(response){
					LoadPost();
				});	
			}
			
			function Share(){
				if(vm.NewPost.length>0){
					var uri  = "/warbook/Profile/NewPost";
					var data = { post: vm.NewPost };
					
					$http.post(uri, data).success(function(response){
						LoadPost();
						vm.NewPost = "";
					});	
				}
			}
			
			function LoadRequest(){
				var uri  = "/warbook/Profile/LoadRequest";	
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.Request = response;
				});
			}
			
			function LoadPost(){
				var uri  = "/warbook/Profile/LoadPostHome";	
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];			
						}
					}
				}
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.Post = response;
				});
			}
		}
})();