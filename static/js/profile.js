(function(){
	'use strict';
	
	angular.module('warbookApp')
		.controller("ProfileController", ProfileController);
		
		ProfileController.$inject = ['$http'];
		
		function ProfileController($http){
			var vm = this;
			
			
			vm.Post = [];
			vm.vAreWeFriends = true;
			vm.NewPost = "";
			vm.ShowRequestMessage = false;
			vm.vRequestSent = false;
			vm.UserId  = 0;
			vm.ProfilePic = 1;
			
			vm.LoadPost = LoadPost;
			vm.LoadInfo = LoadInfo;
			vm.LoadFriends = LoadFriends;
			vm.AreWeFriends = AreWeFriends;
			vm.SendRequest = SendRequest;
			vm.RequestSent = RequestSent;
			vm.Share = Share;
			vm.Up = Up;
			vm.Down = Down;
			
			LoadPost();
			LoadInfo();
			LoadFriends();
			AreWeFriends();
			RequestSent();
			
			$('#autocomplete').autocomplete({
				serviceUrl: '/warbook/Profile/Users',
				onSelect: function (suggestion) {
					location.href = '/warbook/Profile/view?UserID='+suggestion.data;
				}
			});
			
			function Up(PostID){
				var uri  = "/warbook/Profile/Up?PostID="+PostID;
				var data = { };
					
				$http.post(uri, data).success(function(response){
					LoadPost();
				});	
			}
			
			function Down(PostID){
				var uri  = "/warbook/Profile/Down?PostID="+PostID;
				var data = { };
					
				$http.post(uri, data).success(function(response){
					LoadPost();
				});	
			}
			
			function Share(){
				if(vm.NewPost.length>0){
					var uri  = "/warbook/Profile/NewPost";
					var data = { post: vm.NewPost };
					
					$http.post(uri, data).success(function(response){
						LoadPost();
						vm.NewPost = "";
					});	
				}
			}
			
			function SendRequest(){
				var uri  = "/warbook/Profile/SendRequest?FriendID="+ vm.FriendID;
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.ShowRequestMessage = true;
					LoadPost();
					LoadInfo();
					LoadFriends();
					AreWeFriends();
					RequestSent();
				});
			}
			
			function RequestSent(){
				var uri  = "/warbook/Profile/RequestSent";
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];
							var data = {  };
							$http.post(uri, data).success(function(response){
								vm.vRequestSent = response.RequestSent;
							});			
						}
					}
				}
			}
			
			function AreWeFriends(){
				var uri  = "/warbook/Profile/AreWeFriends";
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];
							var data = {  };
							vm.FriendID = NextUriPart[1];
							$http.post(uri, data).success(function(response){
								vm.vAreWeFriends = response.AreWeFriends;
							});			
						}
					}
				}	
			}
			
			function LoadInfo(){
				
				var uri  = "/warbook/Profile/LoadInfo";
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];			
						}
					}
				}	
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.Name = response.FirstName + ' '+response.LastName;
					vm.EMail = response.EmailAddress;
					vm.Gender = (response.Gender == 1)? "Male": "Female";
					vm.Age = response.Age;
					vm.ProfilePic = response.ProfilePic;
					vm.UserID = response.UserID;
				});
			}
			
			function LoadFriends(){
				var uri  = "/warbook/Profile/LoadFriends";	
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];			
						}
					}
				}
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.Friends = response;
				});
			}
			
			function LoadPost(){
				var uri  = "/warbook/Profile/LoadPost";	
				var UriParts = location.href.split("?");
				if(UriParts.length==2){
					var NextUriPart = UriParts[1].split("=");
					if(NextUriPart.length==2){
						if(NextUriPart[0] == "UserID"){
							uri += '?UserID='+NextUriPart[1];			
						}
					}
				}
				var data = {  };
				
				$http.post(uri, data).success(function(response){
					vm.Post = response;
				});
			}
		}
})();