<?php
	
	class ProfileView {
		public function View(){
			template()->buildFromTemplates('Main.html');
			page()->setTitle(Session::singleton()->getUser());
			template()->addTemplateBit('ApplicationContent', 'Profile.html');
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
		
		public function Account(){
			template()->buildFromTemplates('Main.html');
			page()->setTitle(Session::singleton()->getUser());
			template()->addTemplateBit('ApplicationContent', 'Account.html');
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
	
?>