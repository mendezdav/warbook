<?php
	
	class HomeView {
		public function View(){
			template()->buildFromTemplates('Main.html');
			page()->setTitle("Home - TL");
			template()->addTemplateBit('ApplicationContent', 'Home.html');
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
	
?>