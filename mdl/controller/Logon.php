<?php

	import('mdl.model.Logon');
	import('mdl.view.Logon');

	class LogonController extends controller{
		
		public function NewUser(){
			
			$this->view->NewUser();
		}
		
		public function RegisterNewUser(){
			$response = array();
			$response["status_code"] = 200;
			$response["status_message"] = "success";
			$response["data"] = array();
			
			$json = file_get_contents("php://input");
			$postdata = json_decode($json);
			
			$user = $this->model->get_child('user');
			
			$user->get(0);
			$user->EmailAddress = $postdata->EmailAddress;
			$user->FirstName = $postdata->FirstName;
			$user->LastName = $postdata->LastName;
			$user->Gender = $postdata->Gender;
			$user->BirthDate = $postdata->BirthDate;
			$user->AccessPassword = md5($postdata->Password);
			$user->save();
			
			echo json_encode($response);
		}
		
	} 

?>