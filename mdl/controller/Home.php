<?php

	import('mdl.model.Home');
	import('mdl.view.Home');

	class HomeController extends controller{
		
		public function View(){
			if (!Session::singleton()->ValidateSession()) {
				HttpHandler::redirect('/warbook/login/form');
			} else {
				$this->view->View();
			}
			
		}
		
	} 

?>