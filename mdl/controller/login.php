<?php

import('mdl.view.login');
import('mdl.model.login');

class LoginController extends controller {

    public function form() {
        if (!Session::singleton()->ValidateSession()) {
            $this->view->show_form();
        } else {
            HttpHandler::redirect('/warbook/Profile/view');
        }
    }

    public function info() {
        $this->view->show_info();
    }
    
    public function Recovery() {
        $this->view->Recovery();
    }

    public function login() {
        if (empty($_POST)) {
            HttpHandler::redirect('/warbook/login/form');
        } else {
            BM::singleton()->getObject('db')->newConnection(HOST, USER, PASSWORD, DATABASE);
            $usuario = BM::singleton()->getObject('db')->sanitizeData($_POST['usuario']);
            $clave = md5($_POST['clave']);
            $query = "SELECT * FROM User WHERE EmailAddress='{$usuario}' AND AccessPassword='{$clave}'";
            //echo $query;
            BM::singleton()->getObject('db')->executeQuery($query);
            if (BM::singleton()->getObject('db')->getNumRows() > 0) {
                $level = 1;
                Session::singleton()->NewSession($usuario, $level);
                HttpHandler::redirect('/warbook/login/form');
            } else {
                HttpHandler::redirect('/warbook/login/form?error_id=2');
            }
        }
    }

}

?>
