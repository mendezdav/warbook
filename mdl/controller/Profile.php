<?php

	import('mdl.model.Profile');
	import('mdl.view.Profile');

	class ProfileController extends controller{
		
		public function View(){
			if (!Session::singleton()->ValidateSession()) {
				HttpHandler::redirect('/warbook/login/form');
			} else {
				$this->view->View();
			}
		}
		
		public function Account(){
			if (!Session::singleton()->ValidateSession()) {
				HttpHandler::redirect('/warbook/login/form');
			} else {
				$this->view->Account();
			}
		}
		
		public function Upload(){
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$id = $row['UserID'];
			
			$ruta = "static/img/";
			
			$foto = $_FILES['imagen']['tmp_name'];
			$nom_archivo = $_FILES['imagen']['name'];
			//$ext = pathinfo($nom_archivo);
			
			//$array = explode(".", $nom_archivo); //Lo uso para dividir el nombre
			
			//$nombre= $array[0]."_".$id.".".$array[1]; //Aqui formo el nuevo nombre
			
			$nombre = "profile_".$id.".jpg";
			$subir = move_uploaded_file($foto, "$ruta/$nombre");
			
			if($subir){
				$query = "update user set ProfilePic =2,imagen='$nombre' where UserID='$id'";
				data_model()->executeQuery($query);
				HttpHandler::redirect("/warbook/Profile/view");
			}
		}
		
		public function SendRequest(){
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			$FriendID = $_GET['FriendID'];
			
			$query = "SELECT * FROM FriendRequest WHERE RequesterID=$FriendID AND TargetUserID=$UserID";
			data_model()->executeQuery($query);
			if(data_model()->getNumRows()>0){
				
				$query = "DELETE FROM FriendRequest WHERE RequesterID=$FriendID AND TargetUserID=$UserID";
				data_model()->executeQuery($query);
				
				$query = "INSERT INTO FriendList(UserID, FriendID, FriendshipTimeStamp) VALUES($UserID, $FriendID, NOW())";
				data_model()->executeQuery($query);
				$query = "INSERT INTO FriendList(UserID, FriendID, FriendshipTimeStamp) VALUES($FriendID, $UserID, NOW())";
				data_model()->executeQuery($query);
					
			}else{
				$query = "INSERT INTO FriendRequest(RequesterID, TargetUserID, FriendRequestTimeStamp) VALUES ($UserID, $FriendID, NOW())";
			
				data_model()->executeQuery($query);
			}
			echo json_encode(array("msg"=>""));
		}
		
		public function LoadInfo(){
			if(isset($_GET['UserID'])&&!empty($_GET['UserID'])){
				$UserID = $_GET['UserID'];
			}else{
				$EmailAddress = Session::singleton()->getUser();
				$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
				data_model()->executeQuery($query);
				$row = data_model()->getResult()->fetch_assoc();
				$UserID = $row['UserID'];
			}
			
			$query="SELECT *, YEAR(CURDATE())-YEAR(BirthDate) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(BirthDate,'%m-%d'), 0, -1) AS Age  FROM user WHERE UserID='$UserID'";
			data_model()->executeQuery($query);
			$response = array();
			while($row= data_model()->getResult()->fetch_assoc()):
				$response =$row;
			endwhile;
			
			echo json_encode($response);
		}
		
		public function AreWeFriends(){
			
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$response = array("AreWeFriends"=>false);
			$FriendID = $_GET['UserID'];
			
			$query = "SELECT * FROM FriendList WHERE UserID = $UserID AND FriendID = $FriendID";
			
			data_model()->executeQuery($query);
			
			if(data_model()->getNumRows()>0){
				$response["AreWeFriends"] = true;
			}
			
			echo json_encode($response);
		}
		
		public function NewPost(){
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);
			$post = $request->post;
			
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$query = "INSERT INTO post(UserID, Content, PostTimeStamp) VALUES($UserID, '$post', NOW())";
			
			data_model()->executeQuery($query);
			
			echo json_encode(array("msg"=>""));
		}
		
		public function Up(){
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			$PostID = $_GET['PostID'];
			$query="SELECT SupportID, PostID, UserID FROM support WHERE UserID = '$UserID' AND PostID='$PostID'";
			data_model()->executeQuery($query);
			if(data_model()->getNumRows()==0):
				#no se ha hecho ningun support
				data_model()->executeQuery("INSERT INTO support (SupportID, PostID, UserID) VALUES (NULL, '$PostID', '$UserID')");
			endif;
			echo json_encode(array("msg"=>""));
		}
		
		public function Down(){
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			$PostID = $_GET['PostID'];
			$query="DELETE FROM support WHERE UserID = '$UserID' AND PostID='$PostID'";
			data_model()->executeQuery($query);
			echo json_encode(array("msg"=>""));
		}
		
		public function Users(){
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$vquery = $_GET['query'];
			$query = "SELECT CONCAT(FirstName,' ', LastName) as value, UserID as data FROM User WHERE CONCAT(FirstName,' ', LastName) LIKE '%$vquery%' AND UserID!=$UserID LIMIT 10";
			
			data_model()->executeQuery($query);
			
			$response = array("suggestions"=>array());
			
			while($row = data_model()->getResult()->fetch_assoc()){
				$response["suggestions"][] = $row;
			}
			
			echo json_encode($response);
		}
		
		public function RequestSent(){
			
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$response = array("RequestSent"=>false);
			$FriendID = $_GET['UserID'];
			
			$query = "SELECT * FROM FriendRequest WHERE RequesterID = $UserID AND TargetUserID = $FriendID";
			
			data_model()->executeQuery($query);
			
			if(data_model()->getNumRows()>0){
				$response["RequestSent"] = true;
			}
			
			echo json_encode($response);
		}
		
		public function LoadFriends(){
			/* Lista de contactos de un usuario*/
			if(isset($_GET['UserID'])&&!empty($_GET['UserID'])){
				$UserID = $_GET['UserID'];
			}else{
				$EmailAddress = Session::singleton()->getUser();
				$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
				data_model()->executeQuery($query);
				$row = data_model()->getResult()->fetch_assoc();
				$UserID = $row['UserID'];
			}
			
			$query="SELECT FriendListID, FriendID, FriendshipTimeStamp, FirstName, LastName, ProfilePic FROM friendlist f INNER JOIN user u ON f.FriendID = u.UserID WHERE f.UserID = '$UserID'";
			data_model()->executeQuery($query);
			$response=array();
			while($row= data_model()->getResult()->fetch_assoc()):
				$response[]=$row;
			endwhile;
			
			echo json_encode($response);
		}
		
		public function LoadRequest(){
			/* Lista de contactos de un usuario*/
		
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$query="SELECT FriendRequestID, RequesterID, FriendRequestTimeStamp, FirstName, LastName FROM FriendRequest f INNER JOIN user u ON f.RequesterID = u.UserID WHERE f.TargetUserID = '$UserID'";
			data_model()->executeQuery($query);
			$response=array();
			while($row= data_model()->getResult()->fetch_assoc()):
				$response[]=$row;
			endwhile;
			
			echo json_encode($response);
		}
		
		public function LoadPost(){
			if(isset($_GET['UserID'])&&!empty($_GET['UserID'])){
				$UserID = $_GET['UserID'];
			}else{
				$EmailAddress = Session::singleton()->getUser();
				$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
				data_model()->executeQuery($query);
				$row = data_model()->getResult()->fetch_assoc();
				$UserID = $row['UserID'];
			}
			
			$response = array();
			$query = "SELECT * FROM viewuserpost WHERE UserID = $UserID ORDER BY PostID DESC";
			data_model()->executeQuery($query);
			while($row = data_model()->getResult()->fetch_assoc()){
				$response[] = $row;
			}
			echo json_encode($response);
		}
		
		public function LoadPostHome(){
			
			$EmailAddress = Session::singleton()->getUser();
			$query = "SELECT UserID FROM User WHERE EmailAddress='$EmailAddress'";
			data_model()->executeQuery($query);
			$row = data_model()->getResult()->fetch_assoc();
			$UserID = $row['UserID'];
			
			$query="SELECT FriendID FROM friendlist WHERE UserID = '$UserID'";
			data_model()->executeQuery($query);
			$response=array();
			while($row= data_model()->getResult()->fetch_assoc()):
				$response[]=$row['FriendID'];
			endwhile;
			
			$users = implode(",", $response);
			
			$users .= ",".$UserID;
			
			$response = array();
			$query = "SELECT PostID, v.UserID as UserID,Content, PostTimeStamp, SupportCount, ProfilePic, CONCAT(FirstName, ' ',LastName) as Name FROM viewuserpost v INNER JOIN User u ON v.UserID = u.UserID WHERE v.UserID IN ($users) ORDER BY PostID DESC ";
			data_model()->executeQuery($query);
			while($row = data_model()->getResult()->fetch_assoc()){
				$response[] = $row;
			}
			echo json_encode($response);
		}
		
	} 

?>